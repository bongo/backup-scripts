#!/usr/bin/bash

list=$( echo $HOME/.keepass $HOME/.ssh $HOME/.gnupg $HOME/.local/share/keyrings $HOME/.local/share/hamster-applet/hamster.db $HOME/docs/engagement/dfld $HOME/docs/personal/{tex,banking,Praktika,Rechnungen,crypto,twofactor} $HOME/docs/Steuer $HOME/dev )
archiveName="$(mktemp -d)/important.tar"

echo $list
tar --strip-components=2 -cf $archiveName $list 2> >(grep -v 'socket ignored' >&2)
echo "List of files in Archive:"
echo files are in: $archiveName 
echo "detecting usb stick"

if [ -f /run/media/bongo/*/.flag ]; then
	echo "found stick, copying file"
	cp $archiveName $(dirname /run/media/bongo/*/.flag)/important/
fi

echo "Encrypted backup to ownCloud? [Y/n]"
read bak
if [[ $bak == "n" ]]; then sync;exit 0; fi

gpg --output ${archiveName}.gpg --symmetric $archiveName


echo "syncing..."
sync
echo "done. removing archive"
rm $archiveName
