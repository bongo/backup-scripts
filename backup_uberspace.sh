#!/bin/bash
PATH=/usr/local/bin:$PATH
HOME=/root
pth=/media/hdd/nosync/uberspace
rsync -auz --delete-after bongo@scorpius.uberspace.de:/home/bongo/ $pth/bongo | grep -v 'vanished'; s=${PIPESTATUS[0]}; if [ $s -eq 24 ];then (exit 0);fi
rsync -auz --delete-after bongo@scorpius.uberspace.de:/var/www/virtual/bongo/ $pth/www | grep -v 'vanished'; s=${PIPESTATUS[0]}; if [ $s -eq 24 ];then (exit 0);fi
rsync -auz --delete-after bongo@scorpius.uberspace.de:/mysqlbackup/latest/bongo/ $pth/mysql | grep -v 'vanished'; s=${PIPESTATUS[0]}; if [ $s -eq 24 ];then (exit 0);fi
